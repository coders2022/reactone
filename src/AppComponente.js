import logo from './logo.svg';
import './App.css';
import React from 'react'
import { render } from 'react-dom'

function MeuComponente(props) {
  return (
    <div style={{ padding: '30px', backgroundColor: 'black' }}>
      <button>{props.text}</button>
    </div>
  )
}

function App() {
  return (    
    <div className="App">
      <MeuComponente text="Hello, Coders Let's do this!" />     
    </div>
  );
}

export default App;

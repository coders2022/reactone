// Importar as bibliotecas nativas do React
import React, { useState } from 'react'
import { render } from 'react-dom'

// Importar as bibliotecas que serão instaladas


// Importar as bibliotecas e objetos Locais
import logo from './logo.svg';
import './App.css';

function randomColor() {
  return `#${Math.random()
    .toString(16)
    .substr(-6)}`
}

function Card({ color, children }) {
  return (
    <div
      style={{
        padding: '20px',
        textAlign: 'center',
        color: 'white',
        backgroundColor: color,
      }}
    >
      {children}
    </div>
  )
}

function App() {
  const [color, setColor] = useState('black')

  return (
    <div
      style={{
        padding: '20px',
      }}
    >
      <Card color={color}>
        <input
          type={'button'}
          value={'Se clicar vai troca a cor'}
          onClick={() => setColor(randomColor())}
        />
      </Card>
    </div>
  )
}

export default App;

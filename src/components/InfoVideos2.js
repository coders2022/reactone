import React, { useEffect } from 'react'
import axios from 'axios'
import { Button } from 'primereact/button';
import ReactPlayer from 'react-player'

const baseURL2 = "https://hoarsesaddlebrownvoxels.wcampos.repl.co/videos";

const videos2 = {
  "lista": [
    {
      "titulo": "Naruto",
      "youtube": "https://youtu.be/2Oe6HUgrRlQ"
    },
    {
      "titulo": "Altered",
      "youtube": "https://www.youtube.com/watch?v=BgzUtkWAdvA"
    }
  ]
};

function InfoVideos2({ color, children }) {

  const [post2, setPost2] = React.useState(null);

  useEffect(() => {

    /*
    axios.get(baseURL2).then((response) => {
      setPost2(response.data);
    });
    */
    setPost2(videos2);

  }, [])

  if (!post2) return null;

  return (

    <div className="surface-ground">
      <div className="grid">

        {post2.lista.map((postrow, index) => (
          <div className="col-12 lg:col-4 p-3">
            <div className="shadow-2 border-round surface-card mb-3 h-full flex-column justify-content-between flex">
              <div className="p-4">
                <div className="flex align-items-center">
                  <span className="inline-flex border-circle align-items-center justify-content-center bg-green-100 mr-3" style={{ width: '38px', height: '38px' }}>
                    <i className="pi pi-globe text-xl text-green-600"></i>
                  </span>
                  <span className="text-900 font-medium text-2xl">{postrow.titulo}</span>
                </div>
                <div className="text-900 my-3 text-xl font-medium">Quam adipiscing vitae proin sagittis.</div>
                <p className="mt-0 mb-3 text-700 line-height-3">xx
                  <ReactPlayer url={postrow.youtube} />
                </p>

              </div>
              <div className="px-4 py-3 surface-100 text-right">
                <Button icon="pi pi-arrow-right" label="More" className="p-button-rounded p-button-success" />
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>



  )
}

export default InfoVideos2;

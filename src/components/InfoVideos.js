import React, { useEffect } from 'react'
import axios from 'axios'

const baseURL2 = "https://hoarsesaddlebrownvoxels.wcampos.repl.co/videos";

const lista_exemplo = {
  "lista": [
    {
      "titulo": "Matrix",
      "youtube": "https://youtu.be/_-HvOvavLrk"
    },
    {
      "titulo": "007",
      "youtube": "https://www.youtube.com/watch?v=BgzUtkWAdvA"
    },
    {
      "titulo": "Turma da Monica",
      "youtube": "https://www.youtube.com/watch?v=BgzUtkWAdvA"
    }
  ]
};

function InfoVideos() {

  const [filmes, setFilmes] = React.useState(null);

  useEffect(() => {

    /*
    axios.get(baseURL2).then((response) => {
      setPost2(response.data);
    });
    */
    setFilmes(lista_exemplo);

  }, [])

  if (!filmes) return null;

  return (
    <div>
      <ul>
        {filmes.lista.map((postrow, index) => (
          <li key={index}>{postrow.titulo} - {postrow.youtube}</li>

        ))}
      </ul>

    </div>


  )
}

export default InfoVideos;

import React, { useEffect } from 'react'
import axios from 'axios'

const baseURL = "https://api.apify.com/v2/key-value-stores/TyToNta7jGKkpszMZ/records/LATEST?disableRedirect=true";

function InfoCovid() {

  const [infocovid, setInfoCovid] = React.useState(null);

  useEffect(() => {
    axios.get(baseURL).then((response) => {
      setInfoCovid(response.data);
    });

  }, [])

  if (!infocovid) return null;

  return (
    <div>
      <h1>Informações de Infectados por Estado no Brasil</h1>
      <ul>
        {infocovid.infectedByRegion.map((informacao, index) => (
          <li key={index}>{informacao.state} - {informacao.count}</li>
          

        ))}
      </ul>

    </div>

  )
}

export default InfoCovid;

// Importar as bibliotecas nativas do React
import React, { useState } from 'react'
import { render } from 'react-dom'

// Importar as bibliotecas que serão instaladas

// Importar as bibliotecas e objetos Locais
import logo from './logo.svg';
import './App.css';

import Card from './components/Card';

import InfoCovid from './components/InfoCovid';

import InfoVideos from './components/InfoVideos';
import InfoVideos2 from './components/InfoVideos2';

import { classNames } from 'primereact/utils';
import PrimeReact from 'primereact/api';

import './App.scss';

function randomColor() {
  return `#${Math.random()
    .toString(16)
    .substr(-6)}`
}

function App() {
  const [color, setColor] = useState('black')

  return (
    <div
      style={{
        padding: '40px',
      }}
    >
      <Card color={color}>
        <input
          type={'button'}
          value={'Se clicar vai troca a cor'}
          onClick={() => setColor(randomColor())}
        />
      </Card>


      {/* <InfoVideos />       */}

      <InfoVideos2 /> 
      <InfoCovid /> 

      

    </div>
  )
}

export default App;